/* Теоретичні питання
1. Яке призначення методу event.preventDefault() у JavaScript?

це метод який скасовує дії браузера за умовчанням, що відбувається під час обробки події

2. В чому сенс прийому делегування подій?

щоб додати один обробник подій до батьківського елемента замість додавання окремих обробників до кожного дитячого елемента

3. Які ви знаєте основні події документу та вікна браузера? 

- DOMContentLoaded – браузер повністю завантажив HTML, і дерево
DOM побудовано, але зовнішні ресурси, такі як зображення <img> та
стилі, можуть ще не бути завантажені.
- load – завантажено не тільки HTML, але і всі зовнішні ресурси:
зображення, стилі тощо.
- beforeunload/unload – користувач залишає сторінку.

*/

// Практичне завдання:

// Реалізувати перемикання вкладок (таби) на чистому Javascript.

// Технічні вимоги:

// - У папці tabs лежить розмітка для вкладок. Потрібно, щоб після натискання на вкладку відображався конкретний текст для потрібної вкладки. При цьому решта тексту повинна бути прихована. У коментарях зазначено, який текст має відображатися для якої вкладки.
// - Розмітку можна змінювати, додавати потрібні класи, ID, атрибути, теги.
// - Потрібно передбачити, що текст на вкладках може змінюватись, і що вкладки можуть додаватися та видалятися. При цьому потрібно, щоб функція, написана в джаваскрипті, через такі правки не переставала працювати.

//  Умови:
//  - При реалізації обов'язково використовуйте прийом делегування подій (на весь скрипт обробник подій повинен бути один).

const tabsTitle = document.querySelector('.tabs');

const tabsContent = document.querySelectorAll('.tabs-content li');

tabsContent.forEach(li => li.classList.add('not__show'))


function tabsActive () {

    tabsTitle.addEventListener('click', event => {
        
        
        tabsContent.forEach(li => li.classList.remove('show'))
        let tabsTitleActive = tabsTitle.querySelector('.active');
        
        tabsTitleActive.classList.remove('active')
        event.target.classList.add('active')
        let name = event.target.outerText
        

        for(let i of tabsContent){
            const infoText = i.dataset.info
            if(infoText === name){
                i.classList.add('show')
            }
        }

    })
  
}

tabsActive(tabsTitle)







